#include "mainwindow.h"

#include <QtGui/QApplication>
#include <QSettings>

// .rc file contains possible names
// currently qlog, flog, slog
// put quoted name as next #define
#define APPNAME "qlog"

int main(int argc, char *argv[])
{
    Q_INIT_RESOURCE(resources);
    QApplication app(argc, argv);

    QCoreApplication::setOrganizationName("RefaQtor");
    QCoreApplication::setOrganizationDomain("refaqtor.com");
    QCoreApplication::setApplicationName(APPNAME);
    QSettings::setDefaultFormat(QSettings::IniFormat);

    MainWindow mainWindow;
    mainWindow.setWindowTitle(APPNAME);
    mainWindow.setOrientation(MainWindow::ScreenOrientationAuto);
    mainWindow.showExpanded();

    return app.exec();
}
