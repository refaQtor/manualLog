#ifndef QLOGAPP_H
#define QLOGAPP_H

#include <QObject>
#include <QSqlDatabase>
#include <QDir>
#include <QStringList>
#include <QSettings>
#include <QDateTime>
#include <QSqlRelationalTableModel>
#include <QSortFilterProxyModel>

#include "qlogevent.h"

class QlogApp : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QStringList EventList READ EventList)
    Q_PROPERTY(QSqlRelationalTableModel *TableModel READ TableModel)
    Q_PROPERTY(QSortFilterProxyModel *ProxyModel READ ProxyModel)

    QSettings *Settings;
    QSettings *Config;
    QSqlDatabase QlogDB;
    QStringList m_EventList;


    bool createTables();
    void loadSettings();
    void initializeSettings();
    void openSettingsFiles();

    QSqlRelationalTableModel * m_TableModel;

    QSortFilterProxyModel * m_ProxyModel;

public:
    explicit QlogApp(QObject *parent = 0);
    ~QlogApp();

    bool openDB(QString fname);


    QStringList EventList() const
    {
        return m_EventList;
    }

    QSqlRelationalTableModel * TableModel() const
    {
        return m_TableModel;
    }

    QSortFilterProxyModel * ProxyModel() const
    {
        return m_ProxyModel;
    }

public slots:
    void loadEventListFromDB();
    void logEvent(QlogEvent *eventolog);

};

#endif // QLOGAPP_H
