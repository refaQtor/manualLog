#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QtGui/QMainWindow>
#include <QActionGroup>
#include <QDateTime>
#include <QTimer>
#include <QSqlTableModel>

#include "qlogapp.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
public:
    enum ScreenOrientation {
        ScreenOrientationLockPortrait,
        ScreenOrientationLockLandscape,
        ScreenOrientationAuto
    };

    explicit MainWindow(QWidget *parent = 0);
    virtual ~MainWindow();

    // Note that this will only have an effect on Symbian and Fremantle.
    void setOrientation(ScreenOrientation orientation);

    void showExpanded();

public slots:
    void updateTime();
    void triggerLogAction(int index);

private slots:
    void on_action_Quit_triggered();
    void loadEventWidgets();
    void loadEventCombo();
    void loadLogTable();
    void loadLogFilter();

    void on_action_About_triggered();
    void on_action_Event_triggered();
    void on_action_Log_triggered();

    void on_AddEventButton_clicked();
    void on_QueryCombo_currentIndexChanged(const QString &arg1);
    void on_ExportDataButton_clicked();


signals:
    void eventsChanged();

private:
    Ui::MainWindow *ui;
    QSettings *Settings;
    QlogApp *Qlog;
    QTimer *TimeUpdater;
    QSqlTableModel *DBTable;
};

#endif // MAINWINDOW_H
