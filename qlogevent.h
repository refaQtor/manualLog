#ifndef QLOGEVENT_H
#define QLOGEVENT_H

#include <QObject>
#include <QDateTime>

class QlogEvent : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString Label READ Label WRITE setLabel)
    Q_PROPERTY(QDateTime DateTime READ DateTime WRITE setDateTime)
    Q_PROPERTY(qreal Value READ Value WRITE setValue)
    Q_PROPERTY(QString Note READ Note WRITE setNote)

    QString m_Label;

    QDateTime m_DateTime;

    qreal m_Value;

    QString m_Note;

public:
    explicit QlogEvent(QObject *parent = 0);

    QString Label() const
    {
        return m_Label;
    }

    QDateTime DateTime() const
    {
        return m_DateTime;
    }

    qreal Value() const
    {
        return m_Value;
    }

    QString Note() const
    {
        return m_Note;
    }

signals:

public slots:

    void setLabel(QString arg)
    {
        m_Label = arg;
    }
    void setDateTime(QDateTime arg)
    {
        m_DateTime = arg;
    }
    void setValue(qreal arg)
    {
        m_Value = arg;
    }
    void setNote(QString arg)
    {
        m_Note = arg;
    }
};

#endif // QLOGEVENT_H
