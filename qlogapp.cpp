#include "qlogapp.h"

#include <QMessageBox>
#include <QDesktopServices>
#include <QSqlQuery>
#include <QSqlError>
#include <QDebug>
#include <QCoreApplication>

QlogApp::QlogApp(QObject *parent) :
    QObject(parent),
    Settings(new QSettings())
{
    initializeSettings();
    loadSettings();

    QFileInfo setfile(Settings->fileName());  //FIXME:  application name change isn't changing app db name
    openDB(QDir::fromNativeSeparators(setfile.absolutePath() + "/" + Settings->applicationName() + ".sqlt"));

    m_TableModel = new QSqlRelationalTableModel(this, QlogDB);
    m_TableModel->setTable("event");
    m_TableModel->setEditStrategy(QSqlTableModel::OnManualSubmit);
    m_TableModel->setRelation(1,QSqlRelation("Type","EventID","Label"));
    m_TableModel->select();

    m_ProxyModel = new QSortFilterProxyModel(this);
    m_ProxyModel->setSourceModel(m_TableModel);
    m_ProxyModel->setFilterKeyColumn(1);

    loadEventListFromDB();
}

QlogApp::~QlogApp()
{
    delete Config;
    delete Settings;
    delete m_TableModel;
}

bool QlogApp::openDB(QString fname)
{
    qDebug() << "dbopen fname" << fname;
    QFile dbf(fname);
    QlogDB = QSqlDatabase::addDatabase("QSQLITE");
    QlogDB.setDatabaseName(dbf.fileName());
    if (!dbf.exists())
    {   //if it doesn't exist, create one
        if (QlogDB.open())
        {
            createTables();
        }
        else
        {
            QMessageBox::information(0, tr("ERROR"),
                                     tr("Database failed to open."));
            return false;
        }
    }
    return QlogDB.open();
}

bool QlogApp::createTables()
{
    Config->beginGroup("Events");
    m_EventList = Config->childKeys();

    QSqlQuery createType(QString("CREATE TABLE \"Type\" ( %1 %2")
                         .arg("EventID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,")
                         .arg("Label TEXT)").toAscii());
    qDebug() << "createType error: " << createType.lastError();

    QSqlQuery createEvent(QString("CREATE TABLE \"Event\" ( %1 %2 %3 %4")
                          .arg("Timestamp TEXT,")
                          .arg("EventID INTEGER,")
                          .arg("Note TEXT,")
                          .arg("Value REAL)").toAscii());
    qDebug() << "createEvent error: " << createEvent.lastError();

    for (int c = 0; c < m_EventList.count(); ++c) {
        //        off by one here, eventlist is 0 base, sqlite autoinc starts at 1
        QString label(m_EventList.at(c));
        QSqlQuery eventtype;
        eventtype.prepare("INSERT INTO type (EventID, Label) "
                          "VALUES(:index, :label)");
        eventtype.bindValue(":index",QVariant(QVariant::Int));
        eventtype.bindValue(":label",label);
        qDebug() << "bound : " << eventtype.boundValues();
        eventtype.exec();
        qDebug() << "executed: " << eventtype.executedQuery();
        qDebug() << "inserts " << eventtype.lastError();
        qDebug() << "index : " << c;
    }
    int ret = QMessageBox::information(0, tr("Database Initialization"),
                                       tr("Your database has been initialized\n Restart application."));
    exit(0);
    return true; //WARNING: check query return value and return appropriately
}

void QlogApp::initializeSettings()
{ //app settings

    //window layout & stuff

}

void QlogApp::loadSettings()
{ // user settings
    openSettingsFiles();
    // continue to load settings, knowing they are good

    //load config stuff here
}

void QlogApp::logEvent(QlogEvent *eventolog)
{
    QString getidstring = QString("SELECT EventID FROM Type WHERE Label = '%1'").arg(eventolog->Label());
    QSqlQuery id(getidstring);
    id.first();
    int eid = id.value(0).toInt();  //TODO: need proper rangechecking here
    qDebug() << "get event id: " << id.lastError();
    QSqlQuery newevent;
    newevent.prepare("INSERT INTO Event (Timestamp, EventID, Note, Value)"
                     "Values(:timestamp, :eventid, :note, :value)");
    newevent.bindValue(":timestamp", eventolog->DateTime());
    newevent.bindValue(":eventid", eid);
    newevent.bindValue(":note", eventolog->Note());
    newevent.bindValue(":value", eventolog->Value());
    newevent.exec();
    qDebug() << "insert event: " << newevent.lastError();

    delete eventolog; //??
}

void QlogApp::loadEventListFromDB()
{
    QSqlQuery eventstringlist("SELECT label FROM type");
    qDebug() << "items query : " << eventstringlist.lastError();
    while (eventstringlist.next())
    {
        QString item(eventstringlist.value(0).toString());
        m_EventList.append(item);
    }
}

void QlogApp::openSettingsFiles()
{
    QString appname(QCoreApplication::applicationName());
    QFileInfo setfile(Settings->fileName());
    QFile configfile(QDir::fromNativeSeparators(setfile.absolutePath() + "/" + appname + ".conf"));
    QString configfilepath = configfile.fileName();
    if (!configfile.exists())
    {
        QString resourceini = QString(":/%1/%1/%1.conf").arg(appname);
//        int ret = QMessageBox::information(0, tr("restore config?"),
//                                           tr("%1\n%2").arg(resourceini).arg(configfilepath));
        bool restored = QFile::copy(resourceini,configfilepath);
        if (restored)
        {
            QMessageBox::information(0, tr("Configuration"),
                                     tr("Initializing default file:\n\n %1")
                                     .arg(configfilepath));
            QFile newset(configfilepath); //??
        }
        else
        {
            QMessageBox::information(0, tr("ERROR"),
                                     tr("Failed to open Configuration file."));
            //TODO: do, and notify that all configs are being deleted, initialized on restart,
            exit(1);
        }
    }
    Config = new QSettings("RefaQtor",appname);
}



