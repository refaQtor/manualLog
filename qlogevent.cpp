#include "qlogevent.h"

QlogEvent::QlogEvent(QObject *parent) :
    QObject(parent),
    m_Label(""),
    m_DateTime(QDateTime(QDateTime::currentDateTime())),
    m_Value(0),
    m_Note("")
{
}
