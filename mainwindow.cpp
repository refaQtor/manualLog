#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QCoreApplication>
#include <QDebug>
#include <QDir>
#include <QUrl>
#include <QDesktopServices>

#include "qlogevent.h"
#include "csvmodelwriter.h"


//TODO: (done) filter in log combo
//TODO: (done) export to csv
//TODO: allow unfiltered log display, blank first combo entry by default(if works) else wrap filter labels in wildcard regex *
//TODO: config export location
//TODO: (done) about box
//TODO: help doc launch
//TODO: feedback web launch
//TODO: config events

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent), ui(new Ui::MainWindow),
      Settings(new QSettings()),
      Qlog(new QlogApp(this)),
      TimeUpdater(new QTimer())
{
    ui->setupUi(this);
    loadEventWidgets();

    loadLogTable();
    loadLogFilter();

    connect(TimeUpdater, SIGNAL(timeout()),this, SLOT(updateTime()));
    updateTime();
    TimeUpdater->start(1000*60);

}

MainWindow::~MainWindow()
{
    delete TimeUpdater;
    delete Settings;
    delete Qlog;

    delete ui;
}

void MainWindow::updateTime()
{
    ui->EventDateTime->setDateTime(QDateTime::currentDateTime());

}

void MainWindow::setOrientation(ScreenOrientation orientation)
{
#if defined(Q_OS_SYMBIAN)
    // If the version of Qt on the device is < 4.7.2, that attribute won't work
    if (orientation != ScreenOrientationAuto) {
        const QStringList v = QString::fromAscii(qVersion()).split(QLatin1Char('.'));
        if (v.count() == 3 && (v.at(0).toInt() << 16 | v.at(1).toInt() << 8 | v.at(2).toInt()) < 0x040702) {
            qWarning("Screen orientation locking only supported with Qt 4.7.2 and above");
            return;
        }
    }
#endif // Q_OS_SYMBIAN

    Qt::WidgetAttribute attribute;
    switch (orientation) {
#if QT_VERSION < 0x040702
    // Qt < 4.7.2 does not yet have the Qt::WA_*Orientation attributes
    case ScreenOrientationLockPortrait:
        attribute = static_cast<Qt::WidgetAttribute>(128);
        break;
    case ScreenOrientationLockLandscape:
        attribute = static_cast<Qt::WidgetAttribute>(129);
        break;
    default:
    case ScreenOrientationAuto:
        attribute = static_cast<Qt::WidgetAttribute>(130);
        break;
#else // QT_VERSION < 0x040702
    case ScreenOrientationLockPortrait:
        attribute = Qt::WA_LockPortraitOrientation;
        break;
    case ScreenOrientationLockLandscape:
        attribute = Qt::WA_LockLandscapeOrientation;
        break;
    default:
    case ScreenOrientationAuto:
        attribute = Qt::WA_AutoOrientation;
        break;
#endif // QT_VERSION < 0x040702
    };
    setAttribute(attribute, true);
}

void MainWindow::showExpanded()
{
#if defined(Q_OS_SYMBIAN) || defined(Q_WS_SIMULATOR)
    showFullScreen();
#elif defined(Q_WS_MAEMO_5)
    showMaximized();
#else
    show();
#endif
}

void MainWindow::loadEventWidgets()
{
    loadEventCombo();
}

void MainWindow::on_action_Quit_triggered()
{
    close();
}

void MainWindow::loadEventCombo()
{
    ui->EventCombo->addItems(Qlog->EventList());
    qDebug() << "combo list should be :" << Qlog->EventList();
}

void MainWindow::loadLogTable()
{
    ui->tableView->setModel(Qlog->ProxyModel());
}

void MainWindow::loadLogFilter()
{
//    ui->QueryCombo->addItem("");
    ui->QueryCombo->addItems(Qlog->EventList());

}

void MainWindow::triggerLogAction(int index)
{
    QlogEvent *tolog = new QlogEvent();
    tolog->setLabel(Qlog->EventList().at(index));
    tolog->setDateTime(ui->EventDateTime->dateTime());
    tolog->setValue(ui->EventValue->value());
    tolog->setNote(ui->EventNote->text());
    Qlog->logEvent(tolog);
}

void MainWindow::on_action_Event_triggered()
{
    ui->pages->setCurrentIndex(1);
}

void MainWindow::on_action_Log_triggered()
{
    Qlog->TableModel()->select();
    ui->pages->setCurrentIndex(2);
}

void MainWindow::on_action_About_triggered()
{
    ui->pages->setCurrentIndex(0);
}

void MainWindow::on_AddEventButton_clicked()
{
    triggerLogAction(ui->EventCombo->currentIndex());
}

void MainWindow::on_QueryCombo_currentIndexChanged(const QString &arg1)
{
    Qlog->ProxyModel()->setFilterFixedString(arg1);
}

void MainWindow::on_ExportDataButton_clicked()
{
    QString exportfilename(tr("%1-%2-%3%4")
                           .arg(QCoreApplication::applicationName())
                           .arg(ui->QueryCombo->currentText())
                           .arg(QDateTime::currentDateTime().toString("yyyy.MM.dd.hh.mm"))
                           .arg(".csv"));
    QString exportfilepath(QDir::toNativeSeparators(QDesktopServices::storageLocation(QDesktopServices::DocumentsLocation)+"/"));
    QString exportcomplete(exportfilepath + exportfilename);
    CSVModelWriter csv(exportcomplete);
    csv.setModel(Qlog->ProxyModel());
    csv.write();
    QDesktopServices::openUrl(QUrl("file:///" + exportfilepath));// + exportcomplete)); //just open folder, file handling app start inconsistent
}

